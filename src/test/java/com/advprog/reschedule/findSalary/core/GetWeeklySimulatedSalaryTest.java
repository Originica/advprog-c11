package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.findSalary.service.FindSalaryServiceImpl;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.SessionRepository;
import com.advprog.reschedule.timetable.repository.TeacherRepository;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.lenient;

public class GetWeeklySimulatedSalaryTest {
    private Class<?> getWeeklySimulatedSalaryClass;

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() throws Exception {
        getWeeklySimulatedSalaryClass = Class.forName("com.advprog.reschedule.findSalary.core.GetWeeklySimulatedSalary");
        teacher = new Teacher("1", "Dummy");
    }

    @Test
    public void testGetWeeklySimulatedSalaryTestIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(getWeeklySimulatedSalaryClass.getModifiers()));
    }

    @Test
    public void testGetWeeklySimulatedSalaryTestIsAGetSimulatedSalary() throws Exception {
        Class<?> parentClass = getWeeklySimulatedSalaryClass.getSuperclass();

        assertEquals("com.advprog.reschedule.findSalary.core.GetSimulatedSalary", parentClass.getName());
    }

    @Test
    public void testOverrideNotifyTeacherChange() throws Exception {
        Method notifySimulators = getWeeklySimulatedSalaryClass.getDeclaredMethod("notifyTeacherChange");

        assertEquals(0, notifySimulators.getParameterCount());
    }

    @Test
    public void testOverrideUpdateSimulatedDate() throws Exception {
        Method updateSimulatedDate = getWeeklySimulatedSalaryClass.getDeclaredMethod("updateSimulatedDate");

        assertEquals(0, updateSimulatedDate.getParameterCount());
    }

    @Test
    public void testOverrideCalculateAddition() throws Exception {
        Method calculateAddition = getWeeklySimulatedSalaryClass.getDeclaredMethod("calculateAddition");

        assertEquals("long", calculateAddition.getGenericReturnType().getTypeName());
        assertEquals(0, calculateAddition.getParameterCount());
    }

    @Test
    public void testGetWeeklySimulatedSalary() {
        GetWeeklySimulatedSalary getWeeklySimulatedSalary = new GetWeeklySimulatedSalary();
        getWeeklySimulatedSalary.setCurrentSimulatedSalary(teacher.getTotalSalary());
        long simulatedSalary1 = getWeeklySimulatedSalary.getSalary(teacher);

        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        findSalaryService.getSalary(teacher.getNrg());
        long simulatedSalary2 = findSalaryService.getSimulatedSalary(teacher.getNrg(), "weekly");

        assertEquals(simulatedSalary1, simulatedSalary2);
    }
}
