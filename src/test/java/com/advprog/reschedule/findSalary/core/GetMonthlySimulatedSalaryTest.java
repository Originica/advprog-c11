package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.findSalary.service.FindSalaryServiceImpl;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.TeacherRepository;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.lenient;

public class GetMonthlySimulatedSalaryTest {
    private Class<?> getMonthlySimulatedSalaryClass;

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() throws Exception {
        getMonthlySimulatedSalaryClass = Class.forName("com.advprog.reschedule.findSalary.core.GetMonthlySimulatedSalary");
        teacher = new Teacher("1", "Dummy");
    }

    @Test
    public void testGetMonthlySimulatedSalaryTestIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(getMonthlySimulatedSalaryClass.getModifiers()));
    }

    @Test
    public void testGetMonthlySimulatedSalaryTestIsAGetSimulatedSalary() throws Exception {
        Class<?> parentClass = getMonthlySimulatedSalaryClass.getSuperclass();

        assertEquals("com.advprog.reschedule.findSalary.core.GetSimulatedSalary", parentClass.getName());
    }

    @Test
    public void testOverrideNotifyTeacherChange() throws Exception {
        Method notifySimulators = getMonthlySimulatedSalaryClass.getDeclaredMethod("notifyTeacherChange");

        assertEquals(0, notifySimulators.getParameterCount());
    }

    @Test
    public void testOverrideUpdateSimulatedDate() throws Exception {
        Method updateSimulatedDate = getMonthlySimulatedSalaryClass.getDeclaredMethod("updateSimulatedDate");

        assertEquals(0, updateSimulatedDate.getParameterCount());
    }

    @Test
    public void testOverrideCalculateAddition() throws Exception {
        Method calculateAddition = getMonthlySimulatedSalaryClass.getDeclaredMethod("calculateAddition");

        assertEquals("long", calculateAddition.getGenericReturnType().getTypeName());
        assertEquals(0, calculateAddition.getParameterCount());
    }

    @Test
    public void testGetMonthlySimulatedSalary() {
        GetMonthlySimulatedSalary getMonthlySimulatedSalary = new GetMonthlySimulatedSalary();
        getMonthlySimulatedSalary.setCurrentSimulatedSalary(teacher.getTotalSalary());
        long simulatedSalary1 = getMonthlySimulatedSalary.getSalary(teacher);

        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        findSalaryService.getSalary(teacher.getNrg());
        long simulatedSalary2 = findSalaryService.getSimulatedSalary(teacher.getNrg(), "monthly");

        assertEquals(simulatedSalary1, simulatedSalary2);
    }
}
