package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.findSalary.service.FindSalaryServiceImpl;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.TeacherRepository;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.lenient;

public class GetDailySimulatedSalaryTest {
    private Class<?> getDailySimulatedSalaryClass;

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() throws Exception {
        getDailySimulatedSalaryClass = Class.forName("com.advprog.reschedule.findSalary.core.GetDailySimulatedSalary");
        teacher = new Teacher("1", "Dummy");
    }

    @Test
    public void testGetDailySimulatedSalaryTestIsAConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(getDailySimulatedSalaryClass.getModifiers()));
    }

    @Test
    public void testGetDailySimulatedSalaryTestIsAGetSimulatedSalary() throws Exception {
        Class<?> parentClass = getDailySimulatedSalaryClass.getSuperclass();

        assertEquals("com.advprog.reschedule.findSalary.core.GetSimulatedSalary", parentClass.getName());
    }

    @Test
    public void testOverrideNotifyTeacherChange() throws Exception {
        Method notifySimulators = getDailySimulatedSalaryClass.getDeclaredMethod("notifyTeacherChange");

        assertEquals(0, notifySimulators.getParameterCount());
    }

    @Test
    public void testOverrideNotifySimulators() throws Exception {
        Method notifySimulators = getDailySimulatedSalaryClass.getDeclaredMethod("notifySimulators");

        assertEquals(0, notifySimulators.getParameterCount());
    }

    @Test
    public void testOverrideCalculateAddition() throws Exception {
        Method calculateAddition = getDailySimulatedSalaryClass.getDeclaredMethod("calculateAddition");

        assertEquals("long", calculateAddition.getGenericReturnType().getTypeName());
        assertEquals(0, calculateAddition.getParameterCount());
    }

    @Test
    public void testGetDailySimulatedSalary() {
        GetDailySimulatedSalary getDailySimulatedSalary = new GetDailySimulatedSalary();
        getDailySimulatedSalary.setCurrentSimulatedSalary(teacher.getTotalSalary());
        long simulatedSalary1 = getDailySimulatedSalary.getSalary(teacher);

        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        findSalaryService.getSalary(teacher.getNrg());
        long simulatedSalary2 = findSalaryService.getSimulatedSalary(teacher.getNrg(), "daily");

        assertEquals(simulatedSalary1, simulatedSalary2);
    }
}
