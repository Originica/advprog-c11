package com.advprog.reschedule.findSalary.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GetSimulatedSalaryTest {
    private Class<?> getSimulatedSalaryClass;

    @BeforeEach
    public void setUp() throws Exception {
        getSimulatedSalaryClass = Class.forName("com.advprog.reschedule.findSalary.core.GetSimulatedSalary");
    }

    @Test
    public void testGetSimulatedSalaryIsAnAbstractClass() {
        assertTrue(Modifier.isAbstract(getSimulatedSalaryClass.getModifiers()));
    }
}
