package com.advprog.reschedule.findSalary.service;

import com.advprog.reschedule.findSalary.core.GetDailySimulatedSalary;
import com.advprog.reschedule.findSalary.core.GetMonthlySimulatedSalary;
import com.advprog.reschedule.findSalary.core.GetWeeklySimulatedSalary;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class FindSalaryServiceImplTest {
    @InjectMocks
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private FindSalaryServiceImpl findSalaryService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher("1", "Dummy");
    }

    @Test
    public void testGetSalary() {
        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        long salary = findSalaryService.getSalary(teacher.getNrg());

        assertEquals(salary, 0);
    }
}
