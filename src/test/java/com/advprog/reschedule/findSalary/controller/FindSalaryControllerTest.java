package com.advprog.reschedule.findSalary.controller;

import com.advprog.reschedule.findSalary.service.FindSalaryServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = FindSalaryController.class)
class FindSalaryControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private FindSalaryServiceImpl findSalaryService;

    @Test
    public void testControllerAddSalaryDaily() throws Exception {
        mvc.perform(get("/findSalary/add-daily?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("salary"))
                .andExpect(model().attributeExists("teacherNrg"))
                .andExpect(view().name("findSalary/salary"));
    }

    @Test
    public void testControllerAddSalaryWeekly() throws Exception {
        mvc.perform(get("/findSalary/add-weekly?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("salary"))
                .andExpect(model().attributeExists("teacherNrg"))
                .andExpect(view().name("findSalary/salary"));
    }

    @Test
    public void testControllerAddSalaryMonthly() throws Exception {
        mvc.perform(get("/findSalary/add-monthly?nrg=1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("salary"))
                .andExpect(model().attributeExists("teacherNrg"))
                .andExpect(view().name("findSalary/salary"));
    }
}