package com.advprog.reschedule.timetable.addSalary.controller;

import com.advprog.reschedule.timetable.addSalary.service.*;
import com.advprog.reschedule.timetable.table.service.*;
import com.advprog.reschedule.timetable.repository.*;
import com.advprog.reschedule.timetable.model.*;

import java.util.*;
import java.text.*;
import java.time.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = AddSalaryController.class)
public class AddSalaryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    ClassesRepository classesRepository;

    @Mock
    SessionRepository sessionRepository;

    @Mock
    TeacherRepository teacherRepository;

    @MockBean
    ClassesServiceImpl classesService;

    @MockBean
    AddSalaryServiceImpl addSalaryService;

    @MockBean
    SessionServiceImpl sessionService;

    @MockBean
    TeacherServiceImpl teacherService;

    private Classes classes;
    private Session session;
    private Teacher teacher;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher();
        session = new Session();
        classes = new Classes();

        teacher.setNrg("123");
        teacher.setName("Paimon");
        teacher.setTotalSalary(0);
        teacher.setWorkOnSessions(new ArrayList<Session>());
        teacher.getWorkOnSessions().add(session);
        session.setCourse("Math");
        session.setDay("Monday");
        session.setTeacher(teacher);
        LocalTime startTime = LocalTime.parse("00:00:00");
        session.setStartTime(startTime);
        LocalTime endTime = LocalTime.parse("01:00:00");
        session.setEndTime(endTime);
        session.setOnClass(classes);

        classes.setName("Class A");
        classes.setAvailableSessions(new ArrayList<Session>());
        classes.getAvailableSessions().add(session);

        classesRepository.save(classes);
        teacherRepository.save(teacher);
        sessionRepository.save(session);
    }

    @Test
    public void testSimulationHomeNoLogin() throws Exception {
        List<Classes> classesList = new ArrayList<>();
        classesList.add(classes);
        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(teacher);

        when(classesService.getListClasses()).thenReturn(classesList);
        when(teacherService.getListTeacher()).thenReturn(teacherList);

        mockMvc.perform(get("/"))
                .andExpect(status().isUnauthorized());
//                .andExpect(model().attributeExists("classes"))
//                .andExpect(model().attributeExists("teachers"))
//                .andExpect(view().name("timetable/simulation"));
    }

    // @Test
    // public void testSimulationNextDay() throws Exception {
    //     mockMvc.perform(post("/next-day"))
    //             .andExpect(status().is3xxRedirection())
    // }
}