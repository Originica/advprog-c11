package com.advprog.reschedule.timetable.addSalary.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.ClassesRepository;
import com.advprog.reschedule.timetable.repository.SessionRepository;
import com.advprog.reschedule.timetable.repository.TeacherRepository;
import com.advprog.reschedule.timetable.table.service.ClassesServiceImpl;
import com.advprog.reschedule.timetable.table.service.SessionServiceImpl;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalTime;
import java.util.*;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class AddSalaryServiceTest {
    @Mock
    ClassesRepository classesRepository;

    @Mock
    SessionRepository sessionRepository;

    @Mock
    TeacherRepository teacherRepository;

    @Mock
    ClassesServiceImpl classesService;

    @Mock
    AddSalaryServiceImpl addSalaryService;

    @Mock
    SessionServiceImpl sessionService;

    @Mock
    TeacherServiceImpl teacherService;

    private Classes className;
    private Session session;
    private Teacher teacher;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher();
        session = new Session();
        className = new Classes();

        teacher.setNrg("123");
        teacher.setName("Paimon");
        teacher.setTotalSalary(0);
        teacher.setWorkOnSessions(new ArrayList<Session>());
        teacher.getWorkOnSessions().add(session);
        session.setCourse("Math");
        session.setDay("Monday");
        session.setTeacher(teacher);
        LocalTime startTime = LocalTime.parse("00:00:00");
        session.setStartTime(startTime);
        LocalTime endTime = LocalTime.parse("01:00:00");
        session.setEndTime(endTime);
        session.setOnClass(className);

        className.setName("Class A");
        className.setAvailableSessions(new ArrayList<Session>());
        className.getAvailableSessions().add(session);

        classesRepository.save(className);
        teacherRepository.save(teacher);
        sessionRepository.save(session);
    }

    @Test
    public void testServiceAddSalary() throws Exception {

        lenient().when(classesService.getClass(className.getName())).thenReturn(className);
        lenient().when(classesService.createClass(className)).thenReturn(className);
        lenient().when(sessionService.createSession(session, session.getTeacher().getNrg(), session.getOnClass().getName())).thenReturn(session);
        lenient().when((teacherService.createTeacher(teacher))).thenReturn(teacher);

        addSalaryService.broadcast("Class A", "Monday");
        addSalaryService.update(session.getId(), teacher.getNrg());

        lenient().when(classesRepository.findByName("Class A")).thenReturn(className);
        lenient().when(teacherRepository.findByNrg(teacher.getNrg())).thenReturn(teacher);
        lenient().when(sessionRepository.findById(session.getId())).thenReturn(session);
        lenient().when(addSalaryService.update(session.getId(), teacher.getNrg())).thenReturn(teacher);
        assertEquals(0, teacher.getTotalSalary());
    }
}