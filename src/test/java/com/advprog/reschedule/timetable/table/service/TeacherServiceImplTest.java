package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.TeacherRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class TeacherServiceImplTest {
    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher("1", "Dummy");
    }

    @Test
    public void testServiceCreateTeacher(){
        lenient().when(teacherService.createTeacher(teacher)).thenReturn(teacher);
    }

    @Test
    public void testGetTeacherByNrg() {
        lenient().when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);
        Teacher fetchedTeacher = teacherService.getTeacher(teacher.getNrg());

        assertEquals(teacher.getNrg(), fetchedTeacher.getNrg());
    }
}