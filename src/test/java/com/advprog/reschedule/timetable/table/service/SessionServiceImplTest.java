package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.SessionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalTime;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

public class SessionServiceImplTest {
    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private ClassesServiceImpl classesService;

    @Mock
    private TeacherServiceImpl teacherService;

    @InjectMocks
    private SessionServiceImpl sessionService;

    private Classes classForTest;

    private Teacher teacherForTest;

    private Session sessionForTest;

    @BeforeEach
    public void setUp() {
        classForTest = new Classes("Kelas A");
        teacherForTest = new Teacher("123456789", "Guru");
        sessionForTest = new Session();
        sessionForTest.setId(0);
        sessionForTest.setCourse("Pemrograman lanjut");
        sessionForTest.setDay("Senin");
        sessionForTest.setStartTime(LocalTime.parse("08:00:00"));
        sessionForTest.setEndTime(LocalTime.parse("09:00:00"));
        sessionForTest.setTeacher(teacherForTest);
        sessionForTest.setOnClass(classForTest);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceCreateSession(){
        when(classesService.getClass(classForTest.getName())).thenReturn(classForTest);
        when(teacherService.getTeacher(teacherForTest.getNrg())).thenReturn(teacherForTest);
        lenient().when(sessionService.createSession(new Session(0, "Pemrograman lanjut", "Senin", "08:00:00", "09:00:00"), teacherForTest.getNrg(), classForTest.getName())).thenReturn(sessionForTest);
    }

    @Test
    public void testServiceGetSession() {
        lenient().when(sessionService.getSessionById(0)).thenReturn(sessionForTest);
        Session resultSession = sessionService.getSessionById(0);
        assertEquals(sessionForTest, resultSession);
    }

    @Test
    public void testServiceUpdateSession() {
        sessionService.createSession(sessionForTest, teacherForTest.getNrg(), classForTest.getName());
        String currentSessionCourse = sessionForTest.getCourse();
        sessionForTest.setCourse("Advanced Programming");

        lenient().when(sessionService.getSessionById(0)).thenReturn(sessionForTest);
        lenient().when(sessionService.updateSession(0, sessionForTest)).thenReturn(sessionForTest);
        Session resultSession = sessionService.getSessionById(0);

        assertNotEquals(currentSessionCourse, resultSession.getCourse());
    }

    @Test
    public void testDeleteSession() {
        sessionService.createSession(sessionForTest, teacherForTest.getNrg(), classForTest.getName());
        sessionService.deleteSessionById(0);
        lenient().when(sessionService.getSessionById(0)).thenReturn(null);
        assertEquals(null, sessionService.getSessionById(0));
    }

}
