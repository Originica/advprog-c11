package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.repository.ClassesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ClassesServiceImplTest {
    @Mock
    private ClassesRepository classesRepository;

    @InjectMocks
    private ClassesServiceImpl classesService;

    private Classes classForTest;

    @BeforeEach
    public void setUp(){
        classForTest = new Classes();
        classForTest.setName("Kelas A");
    }

    @Test
    public void testServiceCreateClasses(){
        lenient().when(classesService.createClass(classForTest)).thenReturn(classForTest);
    }

    @Test
    public void testServiceGetClass(){
        lenient().when(classesService.getClass("Kelas A")).thenReturn(classForTest);
        Classes resultClass = classesService.getClass(classForTest.getName());
        assertEquals(classForTest.getName(), resultClass.getName());
    }

    @Test
    public void testServiceGetListClasses(){
        Iterable<Classes> listClasses = classesRepository.findAll();
        lenient().when(classesService.getListClasses()).thenReturn(listClasses);
        Iterable<Classes> listClassesResult = classesService.getListClasses();
        Assertions.assertIterableEquals(listClasses, listClassesResult);
    }

    @Test
    public void testServiceUpdateClass(){
        classesService.createClass(classForTest);
        String currentName = classForTest.getName();
        classForTest.setName("Kelas B");

        lenient().when(classesService.updateClass(classForTest.getName(), classForTest)).thenReturn(classForTest);
        Classes resultClass = classesService.updateClass(classForTest.getName(), classForTest);

        assertNotEquals(resultClass.getName(), currentName);
    }

    @Test
    public void testServiceDeleteClass(){
        classesService.createClass(classForTest);
        classesService.deleteClass("Kelas A");
        lenient().when(classesService.getClass("Kelas A")).thenReturn(null);
        assertEquals(null, classesService.getClass(classForTest.getName()));
    }
}
