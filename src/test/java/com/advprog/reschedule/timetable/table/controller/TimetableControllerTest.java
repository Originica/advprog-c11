package com.advprog.reschedule.timetable.table.controller;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.table.service.ClassesServiceImpl;
import com.advprog.reschedule.timetable.table.service.SessionServiceImpl;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = TimetableController.class)
class TimetableControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeacherServiceImpl teacherService;

    @MockBean
    private ClassesServiceImpl classesService;

    @MockBean
    private SessionServiceImpl sessionService;

    @Test
    public void testControllerPostTeacher() throws Exception {
        mvc.perform(post("/timetable/newTeacher")
                .param("nrg", "123456789")
                .param("name", "Guru"))
                    .andExpect(handler().methodName("createTeacher"));

        verify(teacherService, times(1)).createTeacher(new Teacher("123456789", "Guru"));
    }

    @Test
    public void testControllerPostClass() throws Exception {
        mvc.perform(post("/timetable/newClass")
                .param("name", "Kelas A"))
                    .andExpect(handler().methodName("createClass"));

        verify(classesService, times(1)).createClass(new Classes("Kelas A"));
    }

    @Test
    public void testControllerPostSession() throws Exception {
        mvc.perform(post("/timetable/newSession")
                .param("course", "Pemrograman lanjut")
                .param("day", "Senin")
                .param("startTime", "08:00:00")
                .param("endTime", "09:00:00")
                .param("nrg", "123456789")
                .param("name", "Kelas A"))
                    .andExpect(handler().methodName("createSession"));

        verify(sessionService, times(1)).createSession(new Session(0, "Pemrograman lanjut", "Senin", "08:00:00", "09:00:00"), "123456789", "Kelas A");
    }
}