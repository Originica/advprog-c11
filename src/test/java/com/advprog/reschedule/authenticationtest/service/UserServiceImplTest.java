package com.advprog.reschedule.authenticationtest.service;

import com.advprog.reschedule.authentication.model.UserRole;
import com.advprog.reschedule.authentication.service.UserServiceImpl;
import com.advprog.reschedule.authentication.model.User;
import com.advprog.reschedule.authentication.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setUsername("Dummy");
        user.setPassword("password");
    }

    @Test
    public void testSignUpUser() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        User newUser = userService.signUpUser(user);
        assertEquals(user.getUsername(), newUser.getUsername());
        assertTrue(passwordEncoder.matches("password", user.getPassword()));
        // assertEquals(userService.getUserByUsername(newUser.getUsername()), newUser);
    }

    @Test
    public void testUpdateUserRole() {
        UserRole userRole = UserRole.TEACHER;
        userService.updateUserRole(userRole, user);
        assertEquals(user.getUserRole(), UserRole.TEACHER);
    }
}
