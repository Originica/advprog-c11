package com.advprog.reschedule.frontend.controller;

import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.table.service.ClassesServiceImpl;
import com.advprog.reschedule.timetable.table.service.TeacherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = FrontendController.class)
public class FrontendControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeacherServiceImpl teacherService;

    @MockBean
    private ClassesServiceImpl classesService;

    private Teacher teacher;

    @BeforeEach
    public void setUp() {
        teacher = new Teacher("1", "Dummy");
    }

    @Test
    public void testControllerGetTimeTable() throws Exception{
        mvc.perform(get("/timetable?className=Kelas A"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("timeTable"))
                .andExpect(model().attributeExists("className"))
                .andExpect(model().attributeExists("classSessions"))
                .andExpect(view().name("timetable/table"));
        verify(classesService, times(1)).getSessionsByName("Kelas A");
    }

    @Test
    public void testControllerGetInputForm() throws Exception{
        mvc.perform(get("/timetable/input"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("dataInput"))
                .andExpect(model().attributeExists("availableClass"))
                .andExpect(model().attributeExists("availableTeacher"))
                .andExpect(view().name("timetable/input"));
        verify(classesService, times(1)).getListClasses();
        verify(teacherService, times(1)).getListTeacher();
    }

    @Test
    public void testControllerSalaryPage() throws Exception {
        when(teacherService.getTeacher(teacher.getNrg())).thenReturn(teacher);

        mvc.perform(get("/teacher/salary?nrg=" + teacher.getNrg()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("salary"))
                .andExpect(view().name("salary/salary"));
    }
}
