package com.advprog.reschedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvprogApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvprogApplication.class, args);
	}

}
