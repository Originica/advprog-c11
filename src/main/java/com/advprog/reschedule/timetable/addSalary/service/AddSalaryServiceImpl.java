package com.advprog.reschedule.timetable.addSalary.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.ClassesRepository;
import com.advprog.reschedule.timetable.table.service.ClassesService;
import com.advprog.reschedule.timetable.table.service.SessionService;
import com.advprog.reschedule.timetable.table.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Service
public class AddSalaryServiceImpl implements AddSalaryService {
    @Autowired
    ClassesService classesService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    private SessionService sessionService;

    @Override
    public void changeDay(String className, String day) {
        broadcast(className, day);

        Classes currentClass = classesService.getClass(className);
        if (currentClass.getCurrentDay().equals("senin")) currentClass.setCurrentDay("selasa");
        else if (currentClass.getCurrentDay().equals("selasa")) currentClass.setCurrentDay("rabu");
        else if (currentClass.getCurrentDay().equals("rabu")) currentClass.setCurrentDay("kamis");
        else if (currentClass.getCurrentDay().equals("kamis")) currentClass.setCurrentDay("jumat");
        else if (currentClass.getCurrentDay().equals("jumat")) currentClass.setCurrentDay("senin");

        classesService.updateClass(className, currentClass);
    }

    @Override
    public void broadcast(String className, String currentDay) {
        Classes currentClass = classesService.getClass(className);
        for(Session session : currentClass.getAvailableSessions()) {
            if(session.getDay().equals(currentDay)) {
                int idSession = session.getId();
                String idTeacher = session.getTeacher().getNrg();
                update(idSession, idTeacher);
            }
        }
    }

    @Override
    public Teacher update(int idSession, String idTeacher) {
        System.out.println("update used");
        Teacher currentTeacher = teacherService.getTeacher(idTeacher);
        Session currentSession = sessionService.getSessionById(idSession);
        LocalTime sessionStartTime = currentSession.getStartTime();
        LocalTime sessionEndTime = currentSession.getEndTime();
        long difference = (int) (ChronoUnit.HOURS.between(sessionStartTime, sessionEndTime));

        currentTeacher.setTotalSalary(currentTeacher.getTotalSalary() + (difference * 100000));
        teacherService.updateTeacher(idTeacher, currentTeacher);

        return currentTeacher;
    }
}
