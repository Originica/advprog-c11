package com.advprog.reschedule.timetable.addSalary.controller;

import com.advprog.reschedule.timetable.table.service.ClassesService;
import com.advprog.reschedule.timetable.table.service.SessionService;
import com.advprog.reschedule.timetable.table.service.TeacherService;
import com.advprog.reschedule.timetable.addSalary.service.AddSalaryService;
import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = "/simulation")
public class AddSalaryController {

    @Autowired
    private ClassesService classesService;

    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private AddSalaryService addSalaryService;

    @GetMapping("")
    public String simulationHome(Model model) {
        model.addAttribute("classes", classesService.getListClasses());
        model.addAttribute("teachers", teacherService.getListTeacher());
        return "timetable/simulation";
    }

    @PostMapping("/next-day")
    public String simulationNextDay(HttpServletRequest request, Model model) {
        String className = request.getParameter("className");
        String currentDay = request.getParameter("currentDay");
        addSalaryService.changeDay(className, currentDay);
        model.addAttribute("classes", classesService.getListClasses());
        model.addAttribute("teachers", teacherService.getListTeacher());
        return "redirect:/simulation";
    }
}