package com.advprog.reschedule.timetable.addSalary.service;

import com.advprog.reschedule.timetable.model.Teacher;

public interface AddSalaryService {
    void changeDay(String className, String day);
    void broadcast(String className, String day);
    Teacher update(int idSession, String idTeacher);
}
