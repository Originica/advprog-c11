package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public Teacher createTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
        return teacher;
    }

    @Override
    public Teacher getTeacher(String nrg) {
        return teacherRepository.findByNrg(nrg);
    }

    @Override
    public Iterable<Teacher> getListTeacher() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher updateTeacher(String nrg, Teacher teacher) {
        teacher.setNrg(nrg);
        teacherRepository.save(teacher);
        return teacher;
    }

    @Override
    public void deleteTeacher(String nrg) {
        Teacher teacher = getTeacher(nrg);
        teacherRepository.delete(teacher);
    }
}
