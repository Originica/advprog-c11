package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.SessionRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
//import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {
    @Autowired
    ClassesService classesService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    SessionRepository sessionRepository;

    @Override
    public Session createSession(Session session, String nrg, String name) {
        Classes sessionClass = classesService.getClass(name);
        Teacher sessionTeacher = teacherService.getTeacher(nrg);
        session.setOnClass(sessionClass);
        session.setTeacher(sessionTeacher);
        sessionRepository.save(session);

        return session;
    }

    @Override
    public Session getSessionById(int id) {
        return sessionRepository.findById(id);
    }

    @Override
    public Session updateSession(int id, Session session) {
        session.setId(id);
        sessionRepository.save(session);
        return session;
    }

    @Override
    public void deleteSessionById(int id) {
        Session session = getSessionById(id);
        sessionRepository.delete(session);
    }

//    private Boolean isTimeColission(Session session, Classes sessionClass, Teacher sessionTeacher) {
//        String sessionDay = session.getDay();
//        LocalTime sessionStartTime = session.getStartTime();
//        LocalTime sessionEndTime = session.getEndTime();
//        List<Session> classSessions = sessionClass.getAvailableSessions();
//        for(Session thisSession : classSessions) {
//            if(thisSession.getDay().equals(sessionDay)) {
//                LocalTime thisSessionStartTime = thisSession.getStartTime();
//                LocalTime thisSessionEndTime = thisSession.getEndTime();
//                if(sessionStartTime.isAfter(thisSessionStartTime) && sessionStartTime.isBefore(thisSessionEndTime) || sessionEndTime.isAfter(thisSessionStartTime) && sessionEndTime.isBefore(thisSessionEndTime) || sessionStartTime.equals(thisSessionStartTime) || sessionEndTime.equals(thisSessionEndTime) || sessionStartTime.isBefore(thisSessionStartTime) && sessionEndTime.isAfter(thisSessionEndTime)) {
//                    return true;
//                }
//            }
//        }
//        List<Session> teacherSessions = sessionTeacher.getWorkOnSessions();
//        for(Session thisSession : teacherSessions) {
//            if(thisSession.getDay().equals(sessionDay)) {
//                LocalTime thisSessionStartTime = thisSession.getStartTime();
//                LocalTime thisSessionEndTime = thisSession.getEndTime();
//                if(sessionStartTime.isAfter(thisSessionStartTime) && sessionStartTime.isBefore(thisSessionEndTime) || sessionEndTime.isAfter(thisSessionStartTime) && sessionEndTime.isBefore(thisSessionEndTime) || sessionStartTime.equals(thisSessionStartTime) || sessionEndTime.equals(thisSessionEndTime) || sessionStartTime.isBefore(thisSessionStartTime) && sessionEndTime.isAfter(thisSessionEndTime)) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }


}
