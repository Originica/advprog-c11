package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Teacher;

public interface TeacherService {
    Teacher createTeacher(Teacher teacher);

    Iterable<Teacher> getListTeacher();

    Teacher getTeacher(String teacher);

    Teacher updateTeacher(String nrg, Teacher teacher);

    void deleteTeacher(String nrg);
}
