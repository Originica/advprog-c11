package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.repository.ClassesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassesServiceImpl implements ClassesService {
    @Autowired
    private ClassesRepository classesRepository;

    @Override
    public Classes createClass(Classes newClass) {
        classesRepository.save(newClass);
        return newClass;
    }

    @Override
    public Classes getClass(String className) {
        return classesRepository.findByName(className);
    }

    @Override
    public Iterable<Classes> getListClasses() {
        return classesRepository.findAll();
    }

    @Override
    public Classes updateClass(String className, Classes newClass) {
        newClass.setName(className);
        classesRepository.save(newClass);
        return newClass;
    }

    @Override
    public void deleteClass(String className) {
        Classes targetClass = getClass(className);
        classesRepository.delete(targetClass);
    }
    
    public List<Session> getSessionsByName(String className) {
        Classes thisClass = getClass(className);
        return thisClass.getAvailableSessions();
    }
}
