package com.advprog.reschedule.timetable.table.controller;

import com.advprog.reschedule.timetable.table.service.ClassesService;
import com.advprog.reschedule.timetable.table.service.SessionService;
import com.advprog.reschedule.timetable.table.service.TeacherService;
import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("timetable")
public class TimetableController {
    @Autowired
    private ClassesService classesService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private SessionService sessionService;

    @PostMapping("/newTeacher")
    public String createTeacher(HttpServletRequest request){
        String nrg = request.getParameter("nrg");
        String name = request.getParameter("name");
        teacherService.createTeacher(new Teacher(nrg, name));
        return "redirect:/timetable/input";
    }

    @PostMapping("/newClass")
    public String createClass(@RequestParam(value = "name") String name){
        classesService.createClass(new Classes(name));
        return "redirect:/timetable/input";
    }

    @PostMapping("/newSession")
    public String createSession(HttpServletRequest request){
        String course = request.getParameter("course");
        String day = request.getParameter("day");
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");
        String nrg = request.getParameter("nrg");
        String name = request.getParameter("name");
        sessionService.createSession(new Session(course, day, startTime, endTime), nrg, name);
        return "redirect:/timetable/input";
    }
}
