package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Session;

public interface SessionService {
    Session createSession(Session session, String nrg, String name);

    Session getSessionById(int id);

    Session updateSession(int id, Session session);

    void deleteSessionById(int id);
}
