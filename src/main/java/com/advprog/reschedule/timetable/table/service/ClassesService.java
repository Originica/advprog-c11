package com.advprog.reschedule.timetable.table.service;

import com.advprog.reschedule.timetable.model.Classes;
import com.advprog.reschedule.timetable.model.Session;

import java.util.List;

public interface ClassesService {
    Classes createClass(Classes newClass);

    Classes getClass(String className);

    Iterable<Classes> getListClasses();

    Classes updateClass(String className, Classes newClass);

    void deleteClass(String className);
    
    List<Session> getSessionsByName(String className);
}
