package com.advprog.reschedule.timetable.repository;

import com.advprog.reschedule.timetable.model.Classes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassesRepository extends JpaRepository<Classes, String> {
    Classes findByName(String name);
}
