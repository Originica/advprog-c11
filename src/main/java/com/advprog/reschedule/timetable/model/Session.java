package com.advprog.reschedule.timetable.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name="session")
@Setter
@Getter
@NoArgsConstructor
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    private int id;

    @Column(nullable = false)
    private String course;

    @Column(nullable = false)
    private String day;

    @Column(nullable = false)
    private LocalTime startTime;

    @Column(nullable = false)
    private LocalTime endTime;

    @ManyToOne
    @JoinColumn(name = "nrg")
    private Teacher teacher;

    @ManyToOne
    @JoinColumn(name = "name")
    private Classes onClass;

    public Session(String course, String day, String startTime, String endTime) {
        this.course = course;
        this.day = day;
        this.startTime = LocalTime.parse(startTime);
        this.endTime = LocalTime.parse(endTime);
    }

    public Session(int id, String course, String day, String startTime, String endTime) {
        this.id = id;
        this.course = course;
        this.day = day;
        this.startTime = LocalTime.parse(startTime);
        this.endTime = LocalTime.parse(endTime);
    }
}
