package com.advprog.reschedule.timetable.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="teacher")
@Setter
@Getter
@NoArgsConstructor
public class Teacher {
    @Id
    @Column(updatable = false, nullable = false)
    private String nrg;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private long totalSalary;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
    private List<Session> workOnSessions;

    public Teacher(String nrg, String name) {
        this.nrg = nrg;
        this.name = name;
        totalSalary = 0;
    }
}
