package com.advprog.reschedule.timetable.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="classes")
@Setter
@Getter
@NoArgsConstructor
public class Classes {
    @Id
    @Column(updatable = false, nullable = false)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "onClass")
    private List<Session> availableSessions;

    @Column(nullable=false)
    private String currentDay;

    public Classes(String name) {
        this.name = name;
        this.currentDay = "senin";
    }
}

