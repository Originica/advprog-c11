package com.advprog.reschedule.frontend.controller;

import com.advprog.reschedule.findSalary.service.FindSalaryService;
import com.advprog.reschedule.timetable.table.service.ClassesService;
import com.advprog.reschedule.timetable.table.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
public class FrontendController {
    @Autowired
    private ClassesService classesService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private FindSalaryService salarySimulatorService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String landingPage() {
        return "landingPage";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/timetable")
    public String timeTable(@RequestParam("className") String className, Model model)
    {
        model.addAttribute("className", className);
        model.addAttribute("classSessions", classesService.getSessionsByName(className));
        return "timetable/table";
    }

    @GetMapping("/timetable/input")
    public String dataInput(Model model) {
        model.addAttribute("availableClass", classesService.getListClasses());
        model.addAttribute("availableTeacher", teacherService.getListTeacher());
        return "timetable/input";
    }

    @GetMapping("/findSalary")
    public String salary(@RequestParam(value = "nrg") String nrg, Model model) {
        long salary = salarySimulatorService.getSalary(nrg);
        LocalDate simulationDate = LocalDate.now();
        model.addAttribute("salary", salary);
        model.addAttribute("teacherNrg", nrg);
        model.addAttribute("simulationDate", simulationDate);

        return "findSalary/salary";
    }
}
