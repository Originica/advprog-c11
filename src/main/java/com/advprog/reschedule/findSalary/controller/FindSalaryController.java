package com.advprog.reschedule.findSalary.controller;

import com.advprog.reschedule.findSalary.service.FindSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
@RequestMapping("findSalary")
public class FindSalaryController {
    @Autowired
    private FindSalaryService findSalaryService;

    @GetMapping("/add-daily")
    public String addSalaryDaily(@RequestParam(value = "nrg") String nrg, Model model) {
        long salary = findSalaryService.getSimulatedSalary(nrg, "daily");
        LocalDate simulationDate = findSalaryService.getSimulationDate();
        model.addAttribute("salary", salary);
        model.addAttribute("teacherNrg", nrg);
        model.addAttribute("simulationDate", simulationDate);

        return "findSalary/salary";
    }

    @GetMapping("/add-weekly")
    public String addSalaryWeekly(@RequestParam(value = "nrg") String nrg, Model model) {
        long salary = findSalaryService.getSimulatedSalary(nrg, "weekly");
        LocalDate simulationDate = findSalaryService.getSimulationDate();
        model.addAttribute("salary", salary);
        model.addAttribute("teacherNrg", nrg);
        model.addAttribute("simulationDate", simulationDate);

        return "findSalary/salary";
    }

    @GetMapping("/add-monthly")
    public String addSalaryMonthly(@RequestParam(value = "nrg") String nrg, Model model) {
        long salary = findSalaryService.getSimulatedSalary(nrg, "monthly");
        LocalDate simulationDate = findSalaryService.getSimulationDate();
        model.addAttribute("salary", salary);
        model.addAttribute("teacherNrg", nrg);
        model.addAttribute("simulationDate", simulationDate);

        return "findSalary/salary";
    }
}
