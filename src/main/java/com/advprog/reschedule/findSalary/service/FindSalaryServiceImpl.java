package com.advprog.reschedule.findSalary.service;

import com.advprog.reschedule.findSalary.core.GetDailySimulatedSalary;
import com.advprog.reschedule.findSalary.core.GetMonthlySimulatedSalary;
import com.advprog.reschedule.findSalary.core.GetSimulatedSalary;
import com.advprog.reschedule.findSalary.core.GetWeeklySimulatedSalary;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.table.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class FindSalaryServiceImpl implements FindSalaryService {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private GetDailySimulatedSalary getDailySimulatedSalary;

    @Autowired
    private GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Autowired
    private GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    public FindSalaryServiceImpl() {}

    public long getSalary(String nrg) {
        Teacher teacher = teacherService.getTeacher(nrg);
        long salary = teacher.getTotalSalary();
        return salary;
    }

    public long getSimulatedSalary(String nrg, String additionType) {
        GetSimulatedSalary getTotalSalary = null;

        switch (additionType) {
            case "daily":
                getTotalSalary = getDailySimulatedSalary;
                break;
            case "weekly":
                getTotalSalary = getWeeklySimulatedSalary;
                break;
            case "monthly":
                getTotalSalary = getMonthlySimulatedSalary;
                break;
        }

        Teacher teacher = teacherService.getTeacher(nrg);
        long totalSalary = getTotalSalary.getSalary(teacher);
        
        return totalSalary;
    }

    @Override
    public LocalDate getSimulationDate() {
        return getDailySimulatedSalary.getSimulatedDate().toLocalDate();
    }
}
