package com.advprog.reschedule.findSalary.service;

import java.time.LocalDate;

public interface FindSalaryService {
    long getSalary(String nrg);

    long getSimulatedSalary(String nrg, String additionType);

    LocalDate getSimulationDate();
}
