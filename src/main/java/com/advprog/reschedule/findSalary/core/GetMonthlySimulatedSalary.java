package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.timetable.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class GetMonthlySimulatedSalary extends GetSimulatedSalary {
    @Autowired
    GetDailySimulatedSalary getDailySimulatedSalary;

    @Autowired
    GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    public GetMonthlySimulatedSalary() {
    }

    @Override
    protected long calculateAddition(Teacher teacher) {
        long weeklyAddition = getWeeklySimulatedSalary.calculateAddition(teacher);
        long totalAddition = weeklyAddition*4;

        LocalDateTime fourWeeksAdditionDate = simulatedDate.plusDays(28);
        LocalDateTime totalAdditionDate = fourWeeksAdditionDate;
        while (totalAdditionDate.getDayOfMonth() != simulatedDate.getDayOfMonth()) {
            totalAddition += getDailySimulatedSalary.calculateAddition(teacher);
            totalAdditionDate = totalAdditionDate.plusDays(1);
        }
        return totalAddition;
    }

    @Override
    protected void notifyTeacherChange() {
        getDailySimulatedSalary.setSimulatedTeacher(simulatedTeacher);
        getWeeklySimulatedSalary.setSimulatedTeacher(simulatedTeacher);
        getDailySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
        getWeeklySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
    }

    @Override
    protected void notifySimulators() {
        getDailySimulatedSalary.setSimulatedDate(simulatedDate);
        getWeeklySimulatedSalary.setSimulatedDate(simulatedDate);
        getDailySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
        getWeeklySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
    }

    @Override
    protected void updateSimulatedDate() {
        LocalDateTime totalAdditionDate = simulatedDate.plusDays(28);
        while (totalAdditionDate.getDayOfMonth() != simulatedDate.getDayOfMonth()) {
            totalAdditionDate = totalAdditionDate.plusDays(1);
        }
        simulatedDate = totalAdditionDate;
    }
}
