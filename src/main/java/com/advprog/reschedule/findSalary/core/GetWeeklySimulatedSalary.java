package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Component
public class GetWeeklySimulatedSalary extends GetSimulatedSalary {
    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    GetDailySimulatedSalary getDailySimulatedSalary;

    @Autowired
    GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    public GetWeeklySimulatedSalary() {
    }

    @Override
    protected long calculateAddition(Teacher teacher) {
        Iterable<Session> sessions = sessionRepository.findByTeacher(teacher);
        int totalSessions = 0;
        for (Session s : sessions) {
            long duration = LocalTime.from(s.getStartTime()).until(s.getEndTime(), ChronoUnit.HOURS);
            totalSessions += duration;
        }
        return SALARY_PER_SESSION*totalSessions;
    }

    @Override
    protected void notifyTeacherChange() {
        getDailySimulatedSalary.setSimulatedTeacher(simulatedTeacher);
        getMonthlySimulatedSalary.setSimulatedTeacher(simulatedTeacher);
        getDailySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
        getMonthlySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
    }

    @Override
    protected void notifySimulators() {
        getDailySimulatedSalary.setSimulatedDate(simulatedDate);
        getMonthlySimulatedSalary.setSimulatedDate(simulatedDate);
        getDailySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
        getMonthlySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
    }

    @Override
    protected void updateSimulatedDate() {
        simulatedDate = simulatedDate.plusDays(7);
    }
}
