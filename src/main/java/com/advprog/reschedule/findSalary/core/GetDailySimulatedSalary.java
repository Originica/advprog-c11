package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.timetable.model.Session;
import com.advprog.reschedule.timetable.model.Teacher;
import com.advprog.reschedule.timetable.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

@Component
public class GetDailySimulatedSalary extends GetSimulatedSalary {
    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    GetWeeklySimulatedSalary getWeeklySimulatedSalary;

    @Autowired
    GetMonthlySimulatedSalary getMonthlySimulatedSalary;

    public GetDailySimulatedSalary() {}

    @Override
    protected long calculateAddition(Teacher teacher) {
        Iterable<Session> sessions = sessionRepository.findByTeacherAndDay(teacher, simulatedDate.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.getDefault()));
        int totalSessions = 0;
        for (Session s : sessions) {
            long duration = LocalTime.from(s.getStartTime()).until(s.getEndTime(), ChronoUnit.HOURS);
            totalSessions += duration;
        }
        return SALARY_PER_SESSION*totalSessions;
    }

    @Override
    protected void notifyTeacherChange() {
        getWeeklySimulatedSalary.setSimulatedTeacher(simulatedTeacher);
        getMonthlySimulatedSalary.setSimulatedTeacher(simulatedTeacher);
        getWeeklySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
        getMonthlySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
    }

    @Override
    protected void notifySimulators() {
        getWeeklySimulatedSalary.setSimulatedDate(simulatedDate);
        getMonthlySimulatedSalary.setSimulatedDate(simulatedDate);
        getWeeklySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
        getMonthlySimulatedSalary.setCurrentSimulatedSalary(currentSimulatedSalary);
    }

    @Override
    protected void updateSimulatedDate() {
        simulatedDate = simulatedDate.plusDays(1);
    }
}
