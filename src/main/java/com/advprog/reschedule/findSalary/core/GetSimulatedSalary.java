package com.advprog.reschedule.findSalary.core;

import com.advprog.reschedule.timetable.model.Teacher;

import java.time.LocalDateTime;

public abstract class GetSimulatedSalary {
    protected long currentSimulatedSalary;
    final protected long SALARY_PER_SESSION = 50;
    final protected LocalDateTime TODAY = LocalDateTime.now();
    protected LocalDateTime simulatedDate;
    protected Teacher simulatedTeacher;

    public long getSalary(Teacher teacher) {
        if (simulatedTeacher == null || !(simulatedTeacher.getNrg().equals(teacher.getNrg()))) {
            setSimulatedDate(TODAY);
            setSimulatedTeacher(teacher);
            setCurrentSimulatedSalary(teacher.getTotalSalary());
            notifyTeacherChange();
        }
        long addition = calculateAddition(teacher);
        setCurrentSimulatedSalary(currentSimulatedSalary + addition);
        updateSimulatedDate();
        notifySimulators();
        return currentSimulatedSalary;
    }

    protected abstract long calculateAddition(Teacher teacher);

    protected abstract void notifyTeacherChange();

    protected abstract void notifySimulators();

    protected abstract void updateSimulatedDate();

    public LocalDateTime getSimulatedDate() {
        return simulatedDate;
    }

    public void setCurrentSimulatedSalary(long newSalary) {
        this.currentSimulatedSalary = newSalary;
    }

    public void setSimulatedTeacher(Teacher teacher) {
        this.simulatedTeacher = teacher;
    }

    public void setSimulatedDate(LocalDateTime simulatedDay) {
        this.simulatedDate = simulatedDay;
    }
}
