package com.advprog.reschedule.authentication.model;

public enum UserRole {
    ADMIN, TEACHER
}
