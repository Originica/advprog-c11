package com.advprog.reschedule.authentication.handler;

import com.advprog.reschedule.authentication.model.User;

abstract class SignUpHandler {

    protected SignUpHandler nextHandler;

    public void setNext(SignUpHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    abstract void handle(User user);
}