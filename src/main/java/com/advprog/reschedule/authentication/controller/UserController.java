package com.advprog.reschedule.authentication.controller;

import com.advprog.reschedule.authentication.model.User;
import com.advprog.reschedule.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login() {
        return "authentication/login";
    }

    @GetMapping("/signUp")
    public String signUp(Model model) {
        model.addAttribute("user", new User());
        return "authentication/signUp";
    }

    @PostMapping("/signUp")
    public String signUp(User user) {
        userService.signUpUser(user);
        return "redirect:/login";
    }
}
