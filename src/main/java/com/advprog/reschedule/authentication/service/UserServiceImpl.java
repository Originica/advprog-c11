package com.advprog.reschedule.authentication.service;

import com.advprog.reschedule.authentication.handler.PasswordEncodeHandler;
import com.advprog.reschedule.authentication.model.User;
import com.advprog.reschedule.authentication.model.UserRole;
import com.advprog.reschedule.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private PasswordEncodeHandler passwordEncodeHandler = new PasswordEncodeHandler();

    @Override
    public User createUser(User user) {
        user.setEnabled(true);
        user.setLocked(false);
        user.setExpired(true);
        userRepository.save(user);
        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        User user = userRepository.findByUsername(username).get();
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        return user.orElseThrow(() -> new UsernameNotFoundException(
                MessageFormat.format("Cannot find user with username {}.", username)));
    }

    public User signUpUser(User user) {
        passwordEncodeHandler.handle(user);
        User newUser = createUser(user);
        return newUser;
    }

    @Override
    public User updateUserRole(UserRole userRole, User user) {
        user.setUserRole(userRole);
        userRepository.save(user);
        return user;
    }

}