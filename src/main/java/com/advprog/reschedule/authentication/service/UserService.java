package com.advprog.reschedule.authentication.service;

import com.advprog.reschedule.authentication.model.User;
import com.advprog.reschedule.authentication.model.UserRole;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User createUser(User user);

    User getUserByUsername (String username);

    User updateUserRole(UserRole userRole, User user);

    User signUpUser(User user);
}