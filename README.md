# Tugas Kelompok Advprog C11

### Nama anggota kelompok
| No. | Nama                      | NPM        |
|-----|---------------------------|------------|
| 1   | Erica Harlin              | 1906351013 |
| 2   | Johanes Jason             | 1906293120 |
| 3   | Muhammad Andre Gunawan    | 1906400021 |
| 4   | Olivia Monica             | 1906350641 |


#### Gambaran Aplikasi

Aplikasi yang kelompok kami buat adalah suatu website yang ditujukan bagi guru-guru di sekolah untuk mengatur jadwal kelas, dimana guru-guru bisa melakukan :
1. Menginput nama guru, kelas, sesi, dan mata pelajarannya
2. Generate secara random jadwal kelas-kelas sesuai input
3. Melihat gaji guru sesuai jadwal mengajar mereka bila dianggap mereka dibayar perjam.

#### Daftar Fitur dan Pembagian Tugas
1. Generate timetable : Erica, Andre, Oliv
2. Gaji guru          : Jason


#### Tahap Pengerjaan
Push ke branch masing-masing dengan format {nama}/{bagian}